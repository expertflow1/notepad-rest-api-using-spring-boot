package com.springboot.notepad.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.notepad.entity.Note;
import com.springboot.notepad.service.NotesService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(NotesController.class)
class NotesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private NotesService notesService;

    @Test
    public void createNoteMethodShouldReturnStatus201() throws Exception {
        Note newNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        when(notesService.create(newNote)).thenReturn(newNote);

        String requestBody = mapper.writeValueAsString(newNote);

        mockMvc.perform(post("/v1/notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createNoteMethodShouldCreateCorrectNote() throws Exception {
        Note newNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        when(notesService.create(newNote)).thenReturn(newNote);

        String requestBody = mapper.writeValueAsString(newNote);

        mockMvc.perform(post("/v1/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteId").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteTitle").value("Test Note"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteText").value("The test note text."));
    }

    @Test
    public void getNoteMethodShouldReturnStatus200IfTheNoteExists() throws Exception {
        Note newNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        when(notesService.findOne(1L)).thenReturn(newNote);

        mockMvc.perform(get("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getNoteMethodShouldReturnStatus400IfTheNoteDoesNotExist() throws Exception {

        when(notesService.findOne(1L)).thenReturn(null);

        mockMvc.perform(get("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void getNoteMesthodShouldReturnCorrectNote() throws Exception {
        Note newNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        when(notesService.findOne(1L)).thenReturn(newNote);

        mockMvc.perform(get("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteId").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteTitle").value("Test Note"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteText").value("The test note text."));

    }

    @Test
    public void getAllNotesMethodShouldReturnStatus200() throws Exception {
        List<Note> noteList = new ArrayList<>();

        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        noteList.add(note);

        when(notesService.findAll()).thenReturn(noteList);

        mockMvc.perform(get("/v1/notes")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAllNoteMethodShouldReturnCorrectNotesList() throws Exception {
        List<Note> noteList = new ArrayList<>();

        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        noteList.add(note);

        when(notesService.findAll()).thenReturn(noteList);

        mockMvc.perform(get("/v1/notes")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].noteId").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].noteTitle").value("Test Note"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].noteText").value("The test note text."));

    }

    @Test
    public void updateNoteMethodShouldReturnStatus200fNoteExists() throws Exception {
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note newNote = Note.builder()
                .noteTitle("Updated Note")
                .build();

        Note updatedNote = Note.builder()
                .noteId(1L)
                .noteTitle("Updated Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.when(notesService.findOne(1L)).thenReturn(note);
        Mockito.when(notesService.update(note, newNote)).thenReturn(updatedNote);


        String requestBody = mapper.writeValueAsString(newNote);

        mockMvc.perform(get("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateNoteMethodShouldReturnStatus400IfNoteDoesNotExist() throws Exception {
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note newNote = Note.builder()
                .noteTitle("Updated Note")
                .build();

        Note updatedNote = Note.builder()
                .noteId(1L)
                .noteTitle("Updated Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.when(notesService.findOne(1L)).thenReturn(null);
        Mockito.when(notesService.update(note, newNote)).thenReturn(updatedNote);

        String requestBody = mapper.writeValueAsString(newNote);

        mockMvc.perform(patch("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void updateNoteMethodShouldReturnUpdatedNote() throws Exception {
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note newNote = Note.builder()
                .noteTitle("Updated Note")
                .build();

        Note updatedNote = Note.builder()
                .noteId(1L)
                .noteTitle("Updated Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.when(notesService.findOne(1L)).thenReturn(note);
        Mockito.when(notesService.update(note, newNote)).thenReturn(updatedNote);

        String requestBody = mapper.writeValueAsString(newNote);

        mockMvc.perform(patch("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.jsonPath("$.noteTitle").value("Updated Note"));
    }

    @Test
    public void deleteNoteMethodShouldReturnStatus204IfItExists() throws Exception {
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.when(notesService.findOne(1L)).thenReturn(note);
        Mockito.doAnswer(i -> null).when(notesService).delete(1L);

        mockMvc.perform(delete("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void deleteNoteMethodShouldReturnStatus400IfItDoesNotExist() throws Exception {

        Mockito.when(notesService.findOne(1L)).thenReturn(null);
        Mockito.doAnswer(i -> null).when(notesService).delete(1L);

        mockMvc.perform(delete("/v1/notes/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}