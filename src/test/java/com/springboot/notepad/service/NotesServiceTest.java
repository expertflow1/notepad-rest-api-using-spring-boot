package com.springboot.notepad.service;

import com.google.gson.Gson;
import com.springboot.notepad.entity.Note;
import com.springboot.notepad.repository.NotesRepository;
import com.springboot.notepad.service.impl.NotesServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import redis.clients.jedis.JedisPooled;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class NotesServiceTest {

    @InjectMocks
    private NotesServiceImpl notesService;

    @Mock
    private NotesRepository notesRepository;

    @Mock
    private JedisPooled jedis;

    @Mock
    private Gson gson;

    @Test
    public void createMethodShouldCreateANote(){
        Note newNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note createdNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.when(notesRepository.save(newNote)).thenReturn(createdNote);

        Note returnedNote = notesService.create(newNote);

        assertThat(returnedNote).isEqualTo(newNote);
    }

    @Test
    public void findOneMethodShouldFindANote() {
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note foundNote = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        String noteJson = "{\"noteId\":\"1\", \"noteTitle\": \"Test Note\", \"noteText\": \"The test note text.\", \"creationTine\":\"null\"}";

        Mockito.when(jedis.jsonGet("note:1")).thenReturn(foundNote);

        Mockito.when(gson.toJson(foundNote)).thenReturn(noteJson);
        Mockito.when(gson.fromJson(noteJson, Note.class)).thenReturn(foundNote);
        Note returnedNote = notesService.findOne(1L);

        assertThat(returnedNote).isEqualTo(note);
    }

    @Test
    public void findAllMethodShouldFindListOfNotes(){
        List<Note> noteList = new ArrayList<>();
        Set<String> keys = new HashSet<>();
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        noteList.add(note);
        keys.add("note:1");

        String noteJson = "{\"noteId\":\"1\", \"noteTitle\": \"Test Note\", \"noteText\": \"The test note text.\", \"creationTine\":\"null\"}";

        Mockito.when(jedis.keys("note:*")).thenReturn(keys);
        Mockito.when(jedis.jsonGet("note:1")).thenReturn(note);

        Mockito.when(gson.toJson(note)).thenReturn(noteJson);
        Mockito.when(gson.fromJson(noteJson, Note.class)).thenReturn(note);

        List<Note> returnedNoteList = notesService.findAll();

        assertThat(returnedNoteList).isEqualTo(noteList);
    }

    @Test
    public void updateMethodShouldUpdateTheNote(){
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note newNote = Note.builder()
                .noteTitle("Updated Note")
                .build();

        Note updatedNote = Note.builder()
                .noteId(1L)
                .noteTitle("Updated Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.when(notesRepository.save(note)).thenReturn(updatedNote);

        Note returnedNote = notesService.update(note, newNote);

        assertThat(returnedNote).isEqualTo(updatedNote);
    }

    @Test
    public void deleteMethodShouldDeleteTheNote(){
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Mockito.doAnswer(invocationOnMock -> {
            note.setNoteId(null);
            note.setNoteTitle(null);
            note.setNoteText(null);
            note.setCreationTime(null);
            return null;
        }).when(notesRepository).deleteById(1L);

        notesService.delete(1L);

        Mockito.verify(notesRepository, times(1)).deleteById(1L);
        assertThat(note).hasAllNullFieldsOrProperties();
    }
}