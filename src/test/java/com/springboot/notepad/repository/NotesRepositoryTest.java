package com.springboot.notepad.repository;

import com.springboot.notepad.entity.Note;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class NotesRepositoryTest {

    @Autowired
    private NotesRepository notesRepository;

    @Test
    public void saveMethodShouldSaveTheNote(){
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        Note savedNote = notesRepository.save(note);

        assertThat(savedNote).isEqualTo(note);
    }

    @Test
    public void findByIdMethodShouldReturnTheCorrectNote(){
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note")
                .noteText("The test note text.")
                .creationTime(null)
                .build();

        notesRepository.save(note);
        Note foundNote = notesRepository.findById(1L).orElse(null);

        assertThat(foundNote).isEqualTo(note);
    }

    @Test
    public void findAllMethodShouldReturnListOfNotes(){
        Note note1 = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note 1")
                .noteText("The test note 1 text.")
                .creationTime(null)
                .build();

        Note note2 = Note.builder()
                .noteId(2L)
                .noteTitle("Test Note 2")
                .noteText("The test note 2 text.")
                .creationTime(null)
                .build();

        notesRepository.save(note1);
        notesRepository.save(note2);

        List<Note> noteList = notesRepository.findAll();

        assertThat(noteList.get(0)).isEqualTo(note1);
        assertThat(noteList.get(1)).isEqualTo(note2);
    }

    @Test
    public void deleteMethodShouldDeleteCorrectNote(){
        Note note = Note.builder()
                .noteId(1L)
                .noteTitle("Test Note 1")
                .noteText("The test note 1 text.")
                .creationTime(null)
                .build();

        notesRepository.save(note);
        notesRepository.deleteById(1L);
        Note foundNote = notesRepository.findById(1L).orElse(null);

        assertThat(foundNote).isNull();
    }
}