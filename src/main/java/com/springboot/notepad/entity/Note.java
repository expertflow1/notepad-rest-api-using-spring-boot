package com.springboot.notepad.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Index;
import org.springframework.data.redis.core.index.Indexed;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "noteId_generator")
    private Long noteId;
    private String noteTitle;
    private String noteText;
    private Date creationTime;
}
