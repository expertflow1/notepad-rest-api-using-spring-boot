package com.springboot.notepad.controller;

import com.springboot.notepad.entity.Note;
import com.springboot.notepad.service.NotesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class NotesController {

    private final NotesService notesService;

    public NotesController(NotesService notesService) {
        this.notesService = notesService;
    }

    @PostMapping("/notes")
    public ResponseEntity<Note> createNote(@RequestBody Note note){
        Note createdNote = notesService.create(note);

        return new ResponseEntity<>(createdNote, HttpStatus.CREATED);
    }


    @GetMapping("/notes")
    public ResponseEntity<List<Note>> getAllNotes(){
        List<Note> noteList = notesService.findAll();

        return new ResponseEntity<>(noteList, HttpStatus.OK);
    }

    @GetMapping("/notes/{noteId}")
    public ResponseEntity<Note> getNote(@PathVariable("noteId") Long noteId){
        Note foundNote = notesService.findOne(noteId);

        if(foundNote == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(foundNote, HttpStatus.OK);
    }

    @PatchMapping("/notes/{noteId}")
    public  ResponseEntity<Note> updateNote(@PathVariable("noteId") Long noteId, @RequestBody Note note){
        Note foundNote = notesService.findOne(noteId);

        if(foundNote == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Note updatedNote = notesService.update(foundNote, note);
        return new ResponseEntity<>(updatedNote, HttpStatus.OK);
    }

    @DeleteMapping("/notes/{noteId}")
    public ResponseEntity<?> deleteNote(@PathVariable("noteId") Long noteId){
        Note foundNote = notesService.findOne(noteId);

        if(foundNote == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        notesService.delete(noteId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
