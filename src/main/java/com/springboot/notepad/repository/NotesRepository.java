package com.springboot.notepad.repository;

import com.springboot.notepad.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("jpaNotesRepository")
public interface NotesRepository extends JpaRepository<Note, Long> {
}
