package com.springboot.notepad.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import redis.clients.jedis.JedisPooled;

@Configuration
public class RedisConfiguration {

    @Bean
    public JedisConnectionFactory connectionFactory(){
        return new JedisConnectionFactory(new RedisStandaloneConfiguration());
    }

    @Bean
    public JedisPooled jedis(){
        return new JedisPooled();
    }
}
