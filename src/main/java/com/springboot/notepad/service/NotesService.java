package com.springboot.notepad.service;

import com.springboot.notepad.entity.Note;

import java.util.List;

public interface NotesService {
    Note create(Note note);

    List<Note> findAll();

    Note findOne(Long id);

    void delete(Long noteId);

    Note update(Note oldNote, Note newNote);
}
