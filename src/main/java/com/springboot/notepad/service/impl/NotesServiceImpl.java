package com.springboot.notepad.service.impl;

import com.google.gson.Gson;
import com.springboot.notepad.entity.Note;
import com.springboot.notepad.repository.NotesRepository;
import com.springboot.notepad.service.NotesService;
import org.springframework.stereotype.Service;
import redis.clients.jedis.JedisPooled;
import redis.clients.jedis.search.*;
import redis.clients.jedis.search.schemafields.TextField;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NotesServiceImpl implements NotesService {

    private final NotesRepository notesRepository;
    private final JedisPooled jedis;
    private final Gson gson;
    public NotesServiceImpl(NotesRepository notesRepository, JedisPooled jedis, Gson gson) {
        this.notesRepository = notesRepository;
        this.jedis = jedis;
        this.gson = gson;
    }

    @Override
    public Note create(Note note) {
        note.setCreationTime(new Date());
        Note savedNote = notesRepository.save(note);

        List<Object> docs = jedis.keys("note:*").stream().map(jedis::jsonGet).toList();

        if(!docs.isEmpty()){
            jedis.jsonSet("note:"+savedNote.getNoteId(), gson.toJson(savedNote));
        }

        return note;
    }

    @Override
    public List<Note> findAll() {
        List<Object> docs = jedis.keys("note:*").stream().map(jedis::jsonGet).toList();

        if(docs.isEmpty()){
            List<Note> noteList = notesRepository.findAll();
            if(noteList.isEmpty()) {
                return noteList;
            }
            else {
                for(Note note: noteList){
                    jedis.jsonSet("note:" + note.getNoteId(), gson.toJson(note));
                }
            }

            docs = jedis.keys("note:*").stream().map(jedis::jsonGet).toList();

        }

        return docs.stream().map(gson::toJson).map(doc->gson.fromJson(doc, Note.class)).collect(Collectors.toList());
    }

    @Override
    public Note findOne(Long id) {

        Object obj =  jedis.jsonGet("note:" + id);

        if(obj == null){
            Note note = notesRepository.findById(id).orElse(null);
            if(note == null) {
                return null;
            }
            else {
                jedis.jsonSet("note:"+note.getNoteId(), gson.toJson(note));
            }

            obj =  jedis.jsonGet("note:" + id);
        }

        String noteJson = gson.toJson(obj);

        return gson.fromJson(noteJson, Note.class);
    }

    @Override
    public void delete(Long noteId) {
        notesRepository.deleteById(noteId);
        jedis.jsonDel("note:"+noteId);
    }

    @Override
    public Note update(Note oldNote, Note newNote) {
        if(Optional.ofNullable(newNote.getNoteText()).isPresent()){
            oldNote.setNoteText(newNote.getNoteText());
        }
        else if(Optional.ofNullable(newNote.getNoteTitle()).isPresent()){
            oldNote.setNoteTitle(newNote.getNoteTitle());
        }
        notesRepository.save(oldNote);

        Object obj = jedis.jsonGet("note:"+oldNote.getNoteId());
        if(obj != null){
            jedis.jsonDel("note:"+oldNote.getNoteId());
            jedis.jsonSet("note:"+oldNote.getNoteId(), gson.toJson(oldNote));
        }

        return oldNote;
    }
}
